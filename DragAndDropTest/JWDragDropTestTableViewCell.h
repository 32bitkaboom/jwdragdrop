//
//  JWDragDropTestTableViewCell.h
//  DragAndDropTest
//
//  Created by John.Watson on 5/16/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import "JWDraggableTableViewCell.h"

//*** JW STEP #3 ***//
@interface JWDragDropTestTableViewCell : JWDraggableTableViewCell

@property UILabel *label;

@end
