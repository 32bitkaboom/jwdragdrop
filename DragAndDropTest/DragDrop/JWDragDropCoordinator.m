//
//  JWDragDropCoordinator.m
//
//  Created by John Watson
//

#import "JWDragDropCoordinator.h"
#import <QuartzCore/QuartzCore.h>
#import "JWDroppableTarget.h"
#import "JWDraggingObject.h"
#import "JWDragDropNotificationConstants.h"

#define DROPPABLEVIEW_PICKUP_ANIMATION_DURATION 0.1
#define DROPPABLEVIEW_RETURN_ANIMATION_DURATION 0.25
#define DROPPABLEVIEW_DROP_SUCCESS_ANIMATION_DURATION 0.25

#define MAX_DROP_TARGET_COUNT 25

@implementation JWDragDropCoordinator

+ (JWDragDropCoordinator *) sharedInstance {
    static JWDragDropCoordinator *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[JWDragDropCoordinator alloc] init];
    });
    return _sharedClient;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.droppableTargets = [[NSArray alloc] initWithObjects:[[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_NONE
                                 [[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_LOWEST
                                 [[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_LOW
                                 [[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_MEDIUM
                                 [[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_HIGH
                                 [[NSMutableSet alloc] initWithCapacity:MAX_DROP_TARGET_COUNT], //JW_DRAG_DROP_Z_POSITION_HIGHEST
                                 nil];
        self.draggingObjects = [[NSMutableDictionary alloc] initWithCapacity:MAX_DROP_TARGET_COUNT];
    }
    return self;
}

- (void) setGlobalCoordinateView:(UIView *)view
{
    _globalCoordinateView = view;
}

- (UIView *) getGlobalCoordinateView
{
    NSAssert(_globalCoordinateView != nil, @"Drag Drop Global Coordinate View has not been set.");

    return _globalCoordinateView;
}

- (void) addDropTarget:(id<JWDroppableTarget>)dropTarget withZPosition:(kJWDragDropZPosition)zPosition
{
    [self.droppableTargets[zPosition] addObject:dropTarget];
}

- (void) removeDropTarget:(id<JWDroppableTarget>)dropTarget
{
    for (NSMutableSet *zSet in self.droppableTargets) {
        [zSet removeObject:dropTarget];
    }
}

- (void) dragStartedForView:(UIView *)view withTouch:(UITouch *)touch andPayload:(id)payload
{
    [self disableExternalTouchResponders];
    
    JWDraggingObject *draggingObject = [self createDraggingObjectForView:view];
    
    [self.globalCoordinateView addSubview:draggingObject.draggableImage];
    [self.globalCoordinateView bringSubviewToFront:draggingObject.draggableImage];
    
    CGPoint touchLocation = [touch locationInView:self.globalCoordinateView];
    // animate into new position
    [UIView animateWithDuration:DROPPABLEVIEW_PICKUP_ANIMATION_DURATION animations:^{
        draggingObject.draggableImage.center = touchLocation;
    }];
    
    //notify all registered targets that dragging has started
    for (NSMutableSet *zSet in self.droppableTargets) {
        for (id<JWDroppableTarget> target in zSet) {
            [self handleDragStartForTarget:target withPayload:payload];
        }
    }
    
    self.draggingObjects[[self getHashForView:view]] = draggingObject;
}

-(NSString *)getHashForView:(UIView *)view
{
    return [NSString stringWithFormat: @"%d", view.hash];
}

-(JWDraggingObject *) createDraggingObjectForView:(id)view
{
    JWDraggingObject *draggingObject = [[JWDraggingObject alloc] init];
    draggingObject.originalView = view;
    draggingObject.currentlyDraggingViewStartFrame = [self getGloballyAdjustedFrameForView:view];
    draggingObject.draggableImage = [self buildDraggableImageFromView:view];
    
    return draggingObject;
}

- (void) dragEndedForView:(UIView *)view withTouch:(UITouch *)touch andPayload:(id)payload
{
    JWDraggingObject *draggingObject = self.draggingObjects[[self getHashForView:view]];
    if( draggingObject == nil || draggingObject.originalView == nil || view != draggingObject.originalView )
        return;
    
    //determine if we have a hit and allow our drop target to do its thing
    id hitTarget = [self checkForIntersectWithDraggingObject:draggingObject];
    
    BOOL dropAccepted = [self handleDropSuccessForTarget:hitTarget withPayload:payload];
    if( hitTarget != nil && dropAccepted )
    {
        [UIView animateWithDuration:DROPPABLEVIEW_DROP_SUCCESS_ANIMATION_DURATION
                         animations:^{
                             draggingObject.draggableImage.transform = CGAffineTransformMakeScale(0.2, 0.2);
                             draggingObject.draggableImage.alpha = 0.2;
                             draggingObject.draggableImage.center = [self getGloballyAdjustedCenterForView:[self getViewForDropTarget:hitTarget]];
                         }
                         completion:^ (BOOL finished){
                             [draggingObject.draggableImage removeFromSuperview];
                             draggingObject.draggableImage = nil;
                             draggingObject.originalView = nil;
                         }];
    }

    //otherwise animate our image back to where it came from
    else
    {
        [UIView animateWithDuration:DROPPABLEVIEW_RETURN_ANIMATION_DURATION
                         animations:^{
                             draggingObject.draggableImage.frame = draggingObject.currentlyDraggingViewStartFrame;
                         }
                         completion:^ (BOOL finished){
                             [draggingObject.draggableImage removeFromSuperview];
                             draggingObject.draggableImage = nil;
                             draggingObject.originalView = nil;
                         }];
    }

    //notify all registered targets that dragging has ended
    for (NSMutableSet *zSet in self.droppableTargets) {
        for (id<JWDroppableTarget> target in zSet) {
            [self handleDragEndForTarget:target withPayload:payload];
        }
    }
    
    [self enableExternalTouchResponders];
    
    [self.draggingObjects removeObjectForKey:[self getHashForView:view]];
}

- (void) dragMovedForView:(UIView *)view withTouch:(UITouch *)touch andPayload:(id)payload
{
    JWDraggingObject *draggingObject = self.draggingObjects[[self getHashForView:view]];
    if( draggingObject == nil || draggingObject.originalView == nil || view != draggingObject.originalView )
        return;
    
    CGPoint touchLocation = [touch locationInView:self.globalCoordinateView];
    // animate into new position
    [UIView animateWithDuration:DROPPABLEVIEW_PICKUP_ANIMATION_DURATION animations:^{
        draggingObject.draggableImage.center = touchLocation;
    }];
    
    id hitTarget = [self checkForIntersectWithDraggingObject:draggingObject];

    if( self.currentlyOverDropTarget == nil && hitTarget != nil )
    {
        //CASE: new item drag in
        [self handleDragInForTarget:hitTarget withPayload:payload];
        self.currentlyOverDropTarget = hitTarget;
    }
    else if( self.currentlyOverDropTarget != nil && hitTarget != nil )
    {
        //CASE: drag off one item onto another
        [self handleDragOutForTarget:self.currentlyOverDropTarget withPayload:payload];
        
        [self handleDragInForTarget:hitTarget withPayload:payload];
        self.currentlyOverDropTarget = hitTarget;
    }
    else if( self.currentlyOverDropTarget != nil && hitTarget == nil )
    {
        //CASE: drag off of an item with no new item to drag into
        [self handleDragOutForTarget:self.currentlyOverDropTarget withPayload:payload];
        self.currentlyOverDropTarget = nil;
    }
}

#pragma mark Drag Event Handler Passthrough Methods
// These methods are just used to check if the drop target has actually implemented a method that will
// respond the the events raised by the Coordinator. If it does we send it through, if not we drop the event
- (void) handleDragStartForTarget:(id<JWDroppableTarget>)target withPayload:(id)payload
{
    if( [target respondsToSelector:@selector(droppableTargetHandleDragStartWithPayload:)] )
        [target droppableTargetHandleDragStartWithPayload:payload];
}

- (void) handleDragEndForTarget:(id<JWDroppableTarget>)target withPayload:(id)payload
{
    if( [target respondsToSelector:@selector(droppableTargetHandleDragEndWithPayload:)] )
        [target droppableTargetHandleDragEndWithPayload:payload];
}

- (void) handleDragInForTarget:(id<JWDroppableTarget>)target withPayload:(id)payload
{
    if( [target respondsToSelector:@selector(droppableTargetHandleDragInWithPayload:)] )
        [target droppableTargetHandleDragInWithPayload:payload];
}

- (void) handleDragOutForTarget:(id<JWDroppableTarget>)target withPayload:(id)payload
{
    if( [target respondsToSelector:@selector(droppableTargetHandleDragOutWithPayload:)] )
        [target droppableTargetHandleDragOutWithPayload:payload];
}

- (BOOL) handleDropSuccessForTarget:(id<JWDroppableTarget>)target withPayload:(id)payload
{
    if( [target respondsToSelector:@selector(droppableTargetHandleDropSuccessWithPayload:)] )
        return [target droppableTargetHandleDropSuccessWithPayload:payload];
    
    return NO;
}

#pragma mark Intersection Methods
- (id) checkForIntersectWithDraggingObject:(JWDraggingObject *)draggingObject
{
    NSMutableArray *dropTargetIntersectionSizes = [[NSMutableArray alloc] initWithCapacity:MAX_DROP_TARGET_COUNT];
    NSMutableArray *dropTargetsArray = [[NSMutableArray alloc] initWithCapacity:MAX_DROP_TARGET_COUNT];
    //work our way downwards through the Z hierarchy
    for( int zIndex = JW_DRAG_DROP_Z_POSITION_HIGHEST; zIndex >= JW_DRAG_DROP_Z_POSITION_NONE; zIndex-- )
    {
        [dropTargetIntersectionSizes removeAllObjects];
        NSMutableSet *zSet = self.droppableTargets[zIndex];
        for (id dropTarget in zSet) {
            UIView *targetView = [self getViewForDropTarget:dropTarget];
            
            float intersectSize = [self frameIntersectionSizeForView:targetView withDraggingObject:draggingObject];
            if( intersectSize > 0 ) //don't add an intersect size unless it actually intersects!
            {
                [dropTargetIntersectionSizes addObject:[NSNumber numberWithFloat:intersectSize]];
                [dropTargetsArray addObject:dropTarget];
            }
        }
        
        if( dropTargetIntersectionSizes.count > 0 )
            return [self getLargestDropTargetIntersectionForTargets:dropTargetsArray withSizes:dropTargetIntersectionSizes];
    }
    
    return nil;
}

- (float) frameIntersectionSizeForView:(UIView *)targetView withDraggingObject:(JWDraggingObject *)draggingObject
{
    CGRect currentlyDraggingItemFrame = [self getGloballyAdjustedFrameForView:draggingObject.draggableImage];
    CGRect targetViewFrame = [self getGloballyAdjustedFrameForView:targetView];
    CGRect intersect = CGRectIntersection(currentlyDraggingItemFrame, targetViewFrame);
    
    float intersectArea = intersect.size.width * intersect.size.height;
    return intersectArea;
}

-(id) getLargestDropTargetIntersectionForTargets:(NSArray *)targets withSizes:(NSArray *)sizes
{
    //the targets and sizes array should be parallel so the value in sizes[i] is the intersection size for targets[i]
    id currentLargestTarget = nil;
    float currentLargestTargetIntersectSize = 0.0f;
    
    for (int x = 0; x < targets.count; x++) {
        float size = [sizes[x] floatValue];
        
        if( size > currentLargestTargetIntersectSize )
        {
            currentLargestTarget = targets[x];
            currentLargestTargetIntersectSize = size;
        }
    }
    
    return currentLargestTarget;
}

#pragma mark External Touch Responders
- (void) disableExternalTouchResponders
{
    [[NSNotificationCenter defaultCenter] postNotificationName:JWDragDropNotificationDisableTouchResponders object:nil];
}

- (void) enableExternalTouchResponders
{
    [[NSNotificationCenter defaultCenter] postNotificationName:JWDragDropNotificationEnableTouchResponders object:nil];
}

#pragma mark Helper Methods
- (CGRect) getGloballyAdjustedFrameForView:(UIView *)view
{
    return [view convertRect:view.bounds toView:self.globalCoordinateView];
}

- (CGPoint) getGloballyAdjustedCenterForView:(UIView *)view
{
    if (view.superview == nil )
        return [view convertPoint:view.center toView:self.globalCoordinateView];
    
    return [view.superview convertPoint:view.center toView:self.globalCoordinateView];
}

- (UIView *)getViewForDropTarget:(id)dropTarget
{
    if( [dropTarget isKindOfClass:[UITableViewCell class]] ) //check for table cells first since they are also UIViews
        return ((UITableViewCell *)dropTarget).contentView;
    else if( [dropTarget isKindOfClass:[UIView class]] )
        return (UIView *)dropTarget;
    else if( [dropTarget isKindOfClass:[UIViewController class]] )
        return ((UIViewController *)dropTarget).view;
    else
    {
        return nil;
        NSAssert(NO, @"Drop Target is not of type UIView or UIViewController");
    }
}

- (UIImageView *)buildDraggableImageFromView:(UIView *)view
{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *cellImageView = [[UIImageView alloc] initWithImage:cellImage];
    cellImageView.frame = [self getGloballyAdjustedFrameForView:view];
    
    cellImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    cellImageView.layer.shadowOpacity = 0.6;
    cellImageView.layer.shadowOffset = CGSizeMake(0, 0);
    cellImageView.layer.shadowRadius = 3;
    
    return cellImageView;
}

@end
