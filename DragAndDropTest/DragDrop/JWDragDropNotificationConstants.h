//
//  JWDragDropNotificationConstants.h
//
//  Created by John Watson
//

//*** JW STEP #2 ***//
extern NSString * const JWDragDropNotificationDisableTouchResponders;
extern NSString * const JWDragDropNotificationEnableTouchResponders;