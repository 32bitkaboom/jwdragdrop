//
//  JWDraggableView.h
//
//  Created by John Watson
//

#import <Foundation/Foundation.h>

@interface JWDraggableTableViewCell : UITableViewCell

@property id payload;
@property double dragStartDelay; //allows the user to set a delay before a drag is initiated

@property BOOL dragEnabled;

@end
