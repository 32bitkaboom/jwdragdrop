//
//  ViewController.m
//  DragAndDropTest
//
//  Created by John Watson on 4/21/13.
//

#import "ViewController.h"
#import "JWDragDropTestTableViewCell.h"
#import "JWDragDropNotificationConstants.h"
#import "JWDragDropCoordinator.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.dataSource = [[NSMutableArray alloc] initWithObjects:@"One", @"Two", @"Three", @"Four", @"Five", @"Six", @"Seven", @"Eight", @"Nine", @"Ten", nil];
    
    //*** JW STEP #1 ***//
    [JWDragDropCoordinator sharedInstance].globalCoordinateView = self.view;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enableScroll) name:JWDragDropNotificationEnableTouchResponders object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableScroll) name:JWDragDropNotificationDisableTouchResponders object:nil];
    
    //*** JW STEP #6 ***//
    [[JWDragDropCoordinator sharedInstance] addDropTarget:self.dropTarget1View withZPosition:JW_DRAG_DROP_Z_POSITION_HIGHEST];
    [[JWDragDropCoordinator sharedInstance] addDropTarget:self.dropTarget2View withZPosition:JW_DRAG_DROP_Z_POSITION_HIGHEST];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:JWDragDropNotificationDisableTouchResponders object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:JWDragDropNotificationDisableTouchResponders object:nil];
    
    //*** JW STEP #6 ***//
    [[JWDragDropCoordinator sharedInstance] removeDropTarget:self.dropTarget1View];
    [[JWDragDropCoordinator sharedInstance] removeDropTarget:self.dropTarget2View];
}

- (void) enableScroll
{
    self.tableView.scrollEnabled = YES;
}

- (void) disableScroll
{
    self.tableView.scrollEnabled = NO;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"SampleCellIdentifier";
    
    JWDragDropTestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if( !cell )
    {
        cell = [[JWDragDropTestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [self configureCell:cell withRowNum:indexPath.row];
    return cell;
}

-(void) configureCell:(JWDragDropTestTableViewCell *)cell withRowNum:(int)rowNum
{
    cell.label.text = [NSString stringWithFormat:@"Cell %@", self.dataSource[rowNum]];
    
    //*** JW STEP #4 ***//
    cell.payload = cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

@end
