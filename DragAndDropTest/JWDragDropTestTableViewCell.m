//
//  JWDragDropTestTableViewCell.m
//  DragAndDropTest
//
//  Created by John.Watson on 5/16/13.
//  Copyright (c) 2013 John Watson. All rights reserved.
//

#import "JWDragDropTestTableViewCell.h"

@implementation JWDragDropTestTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 100, 50)];
        [self addSubview:self.label];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
