This is a simple implementation of a drag-and-drop paradigm for mobile devices.

### Features: ###
* payload context sensitivity (for highlights and acceptance/rejection)
* drag target highlighting
* drop acceptance/rejection